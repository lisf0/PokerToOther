#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "CompareTableView.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto rootNode = CSLoader::createNode("MainScene.csb");

	addChild(rootNode);

	CompareTableView *m_tableView = CompareTableView::create(rootNode);
	addChild(m_tableView);

	Button *btn_deal = (Button*)rootNode->getChildByName("btn_deal");
	Button *btn_claim = (Button*)rootNode->getChildByName("btn_claim");
	Button *btn_post = (Button*)rootNode->getChildByName("btn_post");

	Button *btn_spread = (Button*)rootNode->getChildByName("btn_spread");
	Button *btn_clear = (Button*)rootNode->getChildByName("btn_clear");

	btn_deal->addClickEventListener([=](Ref *ref) {

		float time = 0.0f;
		for (size_t i = 0; i < 5; i++)		//牌的数量
		{
			for (size_t j = 1; j < 5; j++)	//玩家数量
			{
				time += m_tableView->claimPoker(j, time);
				//time += 0.2f;
			}
		}

	});
	btn_claim->addClickEventListener([=](Ref *ref) {
		for (size_t j = 1; j < 5; j++)	//玩家数量
		{
			m_tableView->claimPoker(j);
		}
	});
	btn_post->addClickEventListener([=](Ref *ref) {
		for (size_t j = 1; j < 5; j++)	//玩家数量
			m_tableView->postPoker(j);
	});

	btn_spread->addClickEventListener([=](Ref *ref) {
		for (size_t j = 1; j < 5; j++)	//玩家数量
			m_tableView->spreadPoker(j,5);
	});

	btn_clear->addClickEventListener([=](Ref *ref) {
		m_tableView->clearAllPoker();
	});
	return true;
}
