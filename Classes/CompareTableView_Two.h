#pragma once
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#define CM_HAND_CARD_BEGIN_POS_X 0.10
#define CM_HAND_CARD_BEGIN_X	 300
#define CM_HAND_CARD_Y_POS 70
#define CM_HAND_CARD_MAX_WIDTH 111
#define CM_HAND_CARD_MAX_HEGIT 157

#define CM_HAND_CARD_MIN 12

#define CM_HAND_CARD_MIN_WIDTH 46
#define CM_HAND_CARD_MIN_HEGIT 62

USING_NS_CC;
using namespace cocos2d::ui;
using namespace std;

class CompareTableViewTwo:public Layer
{
public:


	CompareTableViewTwo(void);
	~CompareTableViewTwo(void);

	static CompareTableViewTwo* create(Node *view);

	void initUI();

	//抓牌
	float claimPoker(int posId,float delta=0.0f);

	void postPoker(int posId);

	//刷新手牌
	void flushHandPoker(int posId,int cardNum);	

	void clearAllPoker();

private:
	void otherUserCardAction(int posId,Sprite *sp);

	std::vector<float> getPokersRotations(int posId,int pokerNum);



protected:
	virtual bool init(Node *view);
	void onExit();
	void onEnter();

	string makeRandStr();


private:
	Node	*m_pRoot;				//主容器
	Node	*m_pokerHeap;			//牌堆

	map<int,Vec2>			m_UserCardsPos;			//其他玩家手牌位置
	vector<Vector<Sprite*>> m_otherUserCards;		//其他玩家手牌


};

