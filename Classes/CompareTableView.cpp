#include "CompareTableView.h"

CompareTableView::CompareTableView(void) :
	m_pRoot(nullptr),
	m_pokerHeap(nullptr)
{
}


CompareTableView::~CompareTableView(void)
{
}

CompareTableView* CompareTableView::create(Node *view)
{
	auto pRet = new CompareTableView();
	if (pRet && pRet->init(view))
	{
		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}


bool CompareTableView::init(Node* view)
{
	if (!Layer::init())
	{
		return false;
	}
	m_pRoot = view;
	if (!m_pRoot) return false;
	this->setContentSize(m_pRoot->getContentSize());

	initUI();

	return true;
}

void CompareTableView::onExit()
{
	Layer::onExit();
}

void CompareTableView::onEnter()
{
	Layer::onEnter();
}

string CompareTableView::makeRandStr()
{
	//random();

	//srand((unsigned)time(NULL));
	//int num = rand();
	string str = "Rand" + StringUtils::toString(random());
	CCLOG("%s:%s", __FUNCTION__, str.c_str());
	return str;
}

void CompareTableView::initUI()
{
	// ͨ��plist���뻺��  

	//�ƶ�
	m_pokerHeap = Sprite::create("cm_card_heap.png");

	m_pokerHeap->setPosition(getContentSize().width / 2, getContentSize().height - 75 - m_pokerHeap->getContentSize().height / 2);
	addChild(m_pokerHeap);

	for (int i = 0; i < 5; i++)
	{
		Vector<Sprite*> vec;
		m_otherUserCards.push_back(vec);

		m_spreadCards.push_back(vec);
	}
	for (size_t i = 1; i < 5; i++)
	{
		m_UserCardsPos.insert(pair<int, Vec2>(i, m_pRoot->getChildByName(StringUtils::format("Cards_%d", i))->getPosition()));
	}

}

float CompareTableView::claimPoker(int posId, float delta)
{
	float actionTime = 0.2f;
	getScheduler()->schedule([=](float d) {

		Vec2 pos = m_UserCardsPos.at(posId);
		Vector<Sprite*> vecSpr = m_otherUserCards.at(posId);
		int cardNum = vecSpr.size();

		vector<Point> postions = getPokersPositions(posId, cardNum + 1, pos);
		vector<float> rotations = getPokersRotations(posId, cardNum + 1);
		Vec2 cardAnchor;
		cardAnchor.y = 0.5f;
		cardAnchor.x = 0.0f;
		for (size_t i = 0; i < cardNum; i++)
		{
			Sprite * cardBack = vecSpr.at(i);
			cardBack->runAction(Spawn::create(RotateTo::create(0.1, rotations[i]), MoveTo::create(0.1, postions[i]), NULL));
		}

		Sprite * cardBack = Sprite::create("cm_card_back.png");	//ͼƬĬ�ϳ����
		Size size = cardBack->getContentSize();
		cardBack->setAnchorPoint(cardAnchor);

		cardBack->setRotation(-90.0f);
		if (pos.x > (this->getContentSize().width / 2))	//�ұߵ�
		{
			cardBack->setScaleX(-1.0f);
			cardBack->setRotation(90.0f);
		}
		Point heapPos = m_pokerHeap->getPosition();
		heapPos.y -= cardBack->getContentSize().height / 2;
		cardBack->setPosition(heapPos);

		addChild(cardBack);
		cardBack->runAction(Spawn::create(RotateTo::create(actionTime, rotations[cardNum]), MoveTo::create(actionTime, postions[cardNum]),NULL));
		m_otherUserCards.at(posId).pushBack(cardBack);

		//sp->runAction(Spawn::create(MoveBy::create(0.0f,Vec2(card_rangex,card_rangey)),RotateTo::create(0.0f,card_rota),NULL));


	}, this, 0, 0, delta, false, makeRandStr());

	return actionTime;
}

void CompareTableView::postPoker(int posId)
{
	Vector<Sprite*> vecSpr = m_otherUserCards.at(posId);
	int cardNum = vecSpr.size() - 1;
	Vec2 pos = m_UserCardsPos.at(posId);

	vector<Point> postions = getPokersPositions(posId, cardNum, pos);
	vector<float> rotations = getPokersRotations(posId, cardNum);
	Vec2 cardAnchor;
	cardAnchor.y = 0.5f;
	cardAnchor.x = 0.0f;

	Sprite *card = *(vecSpr.end() - 1);
	m_otherUserCards.at(posId).eraseObject(card);
	card->removeFromParent();

	for (size_t i = 0; i < cardNum; i++)
	{
		Sprite * cardBack = vecSpr.at(i);
		cardBack->runAction(Spawn::create(RotateTo::create(0.1, rotations[i]), MoveTo::create(0.1, postions[i]), NULL));
	}
}

void CompareTableView::spreadPoker(int posId, int cardNum)
{
	Vec2 pos = m_UserCardsPos.at(posId);

	for (size_t i = 0; i < cardNum; i++)
	{
		Sprite * cardBack = Sprite::create("cm_card_back.png");	//ͼƬĬ�ϳ����
		cardBack->setRotation(90.0f);
		cardBack->setPosition(pos);
		cardBack->setVisible(false);

		Point tempPos = pos;

		if (posId<=2)	
		{
			tempPos.x += 20 * i;
			cardBack->setZOrder(i);
		}
		else
		{
			tempPos.x -= 20 * i;
			cardBack->setZOrder(cardNum-i);
		}


		if (i==0)
		{
			cardBack->setScaleY(0.0f);

			

			cardBack->runAction(Sequence::create(DelayTime::create(0.2f),Show::create(), ScaleTo::create(0.2f,1.0f), CallFunc::create([=]() {
				//cardBack->removeFromParent();
			}), nullptr));
		}
		else
		{
			cardBack->runAction(Sequence::create(DelayTime::create(0.4f), Show::create(),MoveTo::create(0.2f*(cardNum-i)+(i*0.1f), tempPos), nullptr));
		}

		addChild(cardBack);

		m_spreadCards.at(posId).pushBack(cardBack);
	}
}


void CompareTableView::flushHandPoker(int posId, int cardNum)
{
	Vec2 pos = m_UserCardsPos.at(posId);
	vector<Point> postions = getPokersPositions(posId, cardNum + 1, pos);
	vector<float> rotations = getPokersRotations(posId, cardNum + 1);
	Vec2 cardAnchor;
	cardAnchor.y = 0.5f;
	cardAnchor.x = 0.0f;

	for (int i = 0; i < cardNum; i++)
	{

		Sprite * cardBack = Sprite::create("cm_card_back.png");	//ͼƬĬ�ϳ����
		Size size = cardBack->getContentSize();
		cardBack->setAnchorPoint(cardAnchor);

		if (pos.x > (this->getContentSize().width / 2))	//�ұߵ�
		{
			cardBack->setScaleX(-1.0f);
		}
		cardBack->setPosition(postions[i]);
		cardBack->setRotation(rotations[i]);

		addChild(cardBack);
		m_otherUserCards.at(posId).pushBack(cardBack);
	}
}

void CompareTableView::clearAllPoker()
{
	for (size_t i = 0; i < m_otherUserCards.size(); i++)
	{
		int cardNum = m_otherUserCards.at(i).size();

		while (cardNum > 0)
		{
			Sprite *card = *m_otherUserCards.at(i).begin();
			m_otherUserCards.at(i).eraseObject(card);
			card->removeFromParent();
			cardNum = m_otherUserCards.at(i).size();
		}
	}


	for (size_t i = 0; i < m_spreadCards.size(); i++)
	{
		int cardNum = m_spreadCards.at(i).size();

		while (cardNum > 0)
		{
			Sprite *card = *m_spreadCards.at(i).begin();
			m_spreadCards.at(i).eraseObject(card);
			card->removeFromParent();
			cardNum = m_spreadCards.at(i).size();
		}
	}

	
}

std::vector<Point> CompareTableView::getPokersPositions(int posId,int cardCount,Point startPos)
{
	std::vector<Point> positions;

	bool isLeft = posId <= 2;
	float range = 20.0f;
	float card_rangey = 0.0f;
	float card_rangex = 0.0f;

	if (cardCount > 1)
	{
		Point pos;
		int center = cardCount / 2;
		for (int i = 0; i < center; i++)
		{
			card_rangey = (center - i)*range;

			if (isLeft)
			{
				if (center == 3 && i == 0)
					card_rangex = -26;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = -12;
				else
					card_rangex = -3;
			}
			else
			{
				if (center == 3 && i == 0)
					card_rangex = 26;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = 12;
				else
					card_rangex = 3;
			}

			pos.x = startPos.x + card_rangex;
			pos.y = startPos.y + card_rangey;
			positions.push_back(pos);
		}
		for (int i = center; i < cardCount; i++)
		{
			card_rangey = (i - center)*-range;
			if (isLeft)
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = -3;
				else if (i - center > 1)card_rangex = -12;
			}
			else
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = 3;
				else if (i - center > 1)card_rangex = 12;
			}
			pos.x = startPos.x + card_rangex;
			pos.y = startPos.y + card_rangey;
			positions.push_back(pos);
		}
	}
	else if (cardCount == 1)
	{
		positions.push_back(startPos);
	}


	return positions;
}

std::vector<float> CompareTableView::getPokersRotations(int posId, int pokerNum)
{
	std::vector<float> rotations;
	float rota = 15.0f;	//ÿ����Ӧ���ƶ��ĽǶ�
	float rota6 = rota - 1.8f;
	bool isLeft = posId<=2;

	if (isLeft)
	{
		rota = -rota;
		rota6 = -rota6;
	}
	if (pokerNum > 1)
	{
		float card_rota = 0.0f;
		int center = pokerNum / 2;

		for (int i = 0; i < center; i++)
		{
			if (center == 3 && i == 0)
			{
				card_rota = (center - i)*rota6;
			}
			else
			{
				card_rota = (center - i)*rota;
			}

			rotations.push_back(card_rota);
		}
		for (int i = center; i < pokerNum; i++)
		{
			card_rota = (i - center)*-rota;
			rotations.push_back(card_rota);
		}
	}
	else if (pokerNum == 1)
	{
		rotations.push_back(0.0f);
	}

	return rotations;
}

Point CompareTableView::getPokersPosition(int posId)
{
	Point desksPos = m_UserCardsPos.at(posId);
	Point pos;
	int cardCount = m_otherUserCards.at(posId).size();

	float range = 30.0f;

	bool isLeft =posId<=2;

	float card_rangey = 0.0f;
	float card_rangex = 0.0f;

	if (isLeft)
	{

	}


	if (cardCount > 1)
	{
		int center = cardCount / 2;
		for (int i = 0; i < center; i++)
		{
			card_rangey = (center - i)*range;

			if (isLeft)
			{
				if (center == 3 && i == 0)
					card_rangex = -31;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = -12;
				else
					card_rangex = -3;
			}
			else
			{
				if (center == 3 && i == 0)
					card_rangex = 31;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = 12;
				else
					card_rangex = 3;
			}
		}
		for (int i = center; i < cardCount; i++)
		{
			card_rangey = (i - center)*-range;
			if (isLeft)
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = -3;
				else if (i - center > 1)card_rangex = -12;
			}
			else
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = 3;
				else if (i - center > 1)card_rangex = 12;
			}
		}
	}
	else if (cardCount == 1)
	{
		pos = desksPos;
	}


	return pos;
}


void CompareTableView::otherUserCardAction(int posId, Sprite *sp)
{
	Vector<Sprite*> sprVec = m_otherUserCards.at(posId);
	Vec2 pos =/*Vec2(100,300); /*/ m_UserCardsPos.at(posId);
	int cardNum = sprVec.size();

	float rota = 15.0f;	//ÿ����Ӧ���ƶ��ĽǶ�
	float anchorx = 0.0f;
	float anchory = 0.5f;
	float range = 30.0f;

	bool isLeft = pos.x < getContentSize().width / 2;

	if (isLeft)
	{
		rota = -15.0f;
		anchorx = 1.0f;
	}
	if (cardNum > 1)
	{

		sp->setAnchorPoint(Vec2(anchorx, anchory));
		sp->setPosition(pos);

		float card_rota = 0.0f;
		float card_rangey = 0.0f;
		float card_rangex = 0.0f;


		int center = cardNum / 2;

		for (int i = 0; i < center; i++)
		{
			Sprite *sp = m_otherUserCards.at(posId).at(i);
			card_rota = (center - i)*rota;
			card_rangey = (center - i)*range;

			if (isLeft)
			{
				if (center == 3 && i == 0)
					card_rangex = -31;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = -12;
				else
					card_rangex = -3;
			}
			else
			{
				if (center == 3 && i == 0)
					card_rangex = 31;
				else if (center == 3 && i == 1 || center == 2 && i == 0)
					card_rangex = 12;
				else
					card_rangex = 3;
			}
			sp->setPosition(sp->getPosition() + Vec2(card_rangex, card_rangey));
			sp->setRotation(card_rota);
			sp->setVisible(true);
			//sp->runAction(Spawn::create(MoveBy::create(0.0f,Vec2(card_rangex,card_rangey)),RotateTo::create(0.0f,card_rota),NULL));
		}
		for (int i = center; i < cardNum; i++)
		{
			Sprite *sp = m_otherUserCards.at(posId).at(i);
			card_rota = (i - center)*-rota;
			card_rangey = (i - center)*-range;

			if (isLeft)
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = -3;
				else if (i - center > 1)card_rangex = -12;
			}
			else
			{
				if (i - center == 0)card_rangex = 0;
				else if (i - center == 1)card_rangex = 3;
				else if (i - center > 1)card_rangex = 12;
			}
			sp->setVisible(true);
			sp->setPosition(sp->getPosition() + Vec2(card_rangex, card_rangey));
			sp->setRotation(card_rota);
			//sp->runAction(Spawn::create(MoveBy::create(0.0f,Vec2(card_rangex,card_rangey)),RotateTo::create(0.0f,card_rota),NULL));
		}
	}
	else if (cardNum == 1)
	{
		Sprite *sp = m_otherUserCards.at(posId).at(0);
		sp->setPosition(pos);
		sp->setAnchorPoint(Vec2(anchorx, anchory));
		sp->setVisible(true);
	}
}