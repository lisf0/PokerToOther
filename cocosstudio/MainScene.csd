<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1136.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Background" ActionTag="953446860" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ctype="SpriteObjectData">
            <Size X="1136.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="568.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="compare_desk_16to9.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cards_1" ActionTag="121147951" Tag="75" IconVisible="True" LeftMargin="122.8597" RightMargin="1013.1403" TopMargin="431.8622" BottomMargin="208.1378" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="122.8597" Y="208.1378" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1082" Y="0.3252" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cards_2" ActionTag="504022484" Tag="76" IconVisible="True" LeftMargin="122.8597" RightMargin="1013.1403" TopMargin="176.0280" BottomMargin="463.9720" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="122.8597" Y="463.9720" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1082" Y="0.7250" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cards_3" ActionTag="-739595120" Tag="77" IconVisible="True" LeftMargin="1010.0000" RightMargin="126.0000" TopMargin="176.0279" BottomMargin="463.9721" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1010.0000" Y="463.9721" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8891" Y="0.7250" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cards_4" ActionTag="1317491258" Tag="78" IconVisible="True" LeftMargin="1010.0000" RightMargin="126.0000" TopMargin="431.8622" BottomMargin="208.1378" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1010.0000" Y="208.1378" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8891" Y="0.3252" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_deal" ActionTag="1947233541" Tag="80" IconVisible="False" LeftMargin="205.9712" RightMargin="793.0288" TopMargin="541.1529" BottomMargin="45.8471" TouchEnable="True" FontSize="24" ButtonText="发牌" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="10" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="137.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.4712" Y="72.3471" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2416" Y="0.1130" />
            <PreSize X="0.1206" Y="0.0828" />
            <TextColor A="255" R="255" G="0" B="0" />
            <NormalFileData Type="Normal" Path="cm_btn_yellow.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_claim" ActionTag="589926676" Tag="81" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="353.9034" RightMargin="645.0966" TopMargin="541.1529" BottomMargin="45.8471" TouchEnable="True" FontSize="24" ButtonText="抓牌" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="137.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="422.4034" Y="72.3471" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3718" Y="0.1130" />
            <PreSize X="0.1206" Y="0.0828" />
            <TextColor A="255" R="255" G="0" B="0" />
            <NormalFileData Type="Normal" Path="cm_btn_green.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_post" ActionTag="-1783111837" Tag="82" IconVisible="False" LeftMargin="501.8356" RightMargin="497.1644" TopMargin="541.1529" BottomMargin="45.8471" TouchEnable="True" FontSize="24" ButtonText="出牌" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="137.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="570.3356" Y="72.3471" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5021" Y="0.1130" />
            <PreSize X="0.1206" Y="0.0828" />
            <TextColor A="255" R="255" G="0" B="0" />
            <NormalFileData Type="Normal" Path="cm_btn_blue.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_spread" ActionTag="-1085764591" Tag="12" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="649.7678" RightMargin="349.2322" TopMargin="541.1529" BottomMargin="45.8471" TouchEnable="True" FontSize="24" ButtonText="展开" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="137.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="718.2678" Y="72.3471" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6323" Y="0.1130" />
            <PreSize X="0.1206" Y="0.0828" />
            <TextColor A="255" R="255" G="0" B="0" />
            <NormalFileData Type="Normal" Path="cm_btn_green.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_clear" ActionTag="-1822802886" Tag="83" IconVisible="False" LeftMargin="797.7000" RightMargin="201.3000" TopMargin="540.6675" BottomMargin="46.3325" TouchEnable="True" FontSize="24" ButtonText="清理" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="137.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="866.2000" Y="72.8325" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7625" Y="0.1138" />
            <PreSize X="0.1206" Y="0.0828" />
            <TextColor A="255" R="255" G="0" B="0" />
            <NormalFileData Type="Normal" Path="cm_btn_yellow.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>